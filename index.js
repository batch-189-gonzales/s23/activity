function Pokemon (name, level) {

    this.name = name;
    this.level = level;
    this.health = 3 * level;
    this.attack = level;

    this.tackle = function(target) {
        console.log(`${this.name} tackled ${target.name}.`);
        console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}.`);
        target.health -= this.attack;
        if (target.health <= 5) {
            target.faint();
        };
    };

    this.faint = function() {
        console.log(`${this.name} fainted.`);
    };

};

let pikachu = new Pokemon ("Pikachu", 16);
let squirtle = new Pokemon("Squirtle", 16);

console.log(pikachu);
console.log(squirtle);

pikachu.tackle(squirtle);
pikachu.tackle(squirtle);
pikachu.tackle(squirtle);